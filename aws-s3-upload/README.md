# aws s3协议大文件上传到minio

### 介绍
使用vue3 element-plus aws-s3 minio springboot 实现大文件的分片上传、断点续传、秒传的功能demo

此代码博客地址：https://blog.csdn.net/qq_43548590/article/details/140413344?spm=1001.2014.3001.5502
### 疑问
在分片后使用aws-s3包查询当前上传后的分片信息一直超时！！！

没有办法换成了minio的包才解决问题

有大佬解决了这个问题求分享！！！！！！！

### 参考
本文根据
https://www.cnblogs.com/jsonq/p/18186340
改造而来。

感谢风希落大佬的分享。
源码地址：
https://gitee.com/jsonqi/minio-spring-web
### 演示
![输入图片说明](image/s3-upload.gif)

### 环境
java 17
node 16 以上
mysql 8
minio

### 安装教程

后端：
1. 创建数据库，导入sql脚本
2. 修改yml文件，将mysql，minio的配置改为你的配置
3. 在minio中创建你在步骤二中配置的桶
4. 运行springboot

前端：

进入到前端项目根目录
``` javascript
// 运行
npm install
npm run dev
```

### 分片流程
![输入图片说明](image/img.png)