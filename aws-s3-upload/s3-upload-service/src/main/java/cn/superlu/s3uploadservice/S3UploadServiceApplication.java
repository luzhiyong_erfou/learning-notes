package cn.superlu.s3uploadservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@MapperScan("cn.superlu.s3uploadservice.mapper")
public class S3UploadServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(S3UploadServiceApplication.class, args);
    }

}
