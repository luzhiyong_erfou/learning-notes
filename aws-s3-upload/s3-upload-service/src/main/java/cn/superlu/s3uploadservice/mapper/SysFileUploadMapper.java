package cn.superlu.s3uploadservice.mapper;

import cn.superlu.s3uploadservice.model.entity.SysFileUpload;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 文件表 Mapper 接口
 */
public interface SysFileUploadMapper extends BaseMapper<SysFileUpload> {

}
