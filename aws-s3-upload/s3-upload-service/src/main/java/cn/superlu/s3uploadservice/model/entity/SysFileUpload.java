package cn.superlu.s3uploadservice.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 文件扩展表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_file_upload")
public class SysFileUpload extends Model<SysFileUpload> {


	@JsonSerialize(using= ToStringSerializer.class)
	@TableId(type = IdType.ASSIGN_ID)
	private Long id;

	/**
	 * 文件上传id
	 */
	private String uploadId;

	/**
	 * 文件计算md5
	 */
	private String md5;

	/**
	 * 文件访问地址
	 */
	private String url;

	/**
	 * 存储桶
	 */
	private String bucket;

	/**
	 * minio中文件名
	 */
	private String object;

	/**
	 * 原始文件名
	 */
	private String originFileName;

	/**
	 * 文件大小
	 */
	private Long size;

	/**
	 * 文件类型
	 */
	private String type;

	/**
	 * 分片大小
	 */
	private Long chunkSize;

	/**
	 * 分片数量
	 */
	private Integer chunkCount;

	/**
	 * 删除标识（0-正常,1-删除）
	 */
	@TableLogic
	private String delFlag;

	/**
	 * 创建时间
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@TableField(fill = FieldFill.INSERT)
	private LocalDateTime createTime;

	/**
	 * 更新人
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String updateBy;

	/**
	 * 更新时间
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private LocalDateTime updateTime;
}
