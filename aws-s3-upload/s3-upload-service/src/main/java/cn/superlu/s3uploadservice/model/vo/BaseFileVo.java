package cn.superlu.s3uploadservice.model.vo;

import cn.superlu.s3uploadservice.constant.FileHttpCodeEnum;
import lombok.Data;

@Data
public class BaseFileVo<T> {

    private Integer uploadCode;

    private String UploadMsg;

    private T data;

    public static <T> BaseFileVo<T> builder(FileHttpCodeEnum codeEnum,T data) {
        BaseFileVo<T> vo = new BaseFileVo<>();
        vo.setUploadCode(codeEnum.getCode());
        vo.setUploadMsg(codeEnum.getMsg());
        vo.setData(data);
        return vo;
    }
}
