package cn.superlu.s3uploadservice.service;

import cn.superlu.s3uploadservice.common.R;
import cn.superlu.s3uploadservice.model.bo.FileUploadInfo;
import cn.superlu.s3uploadservice.model.entity.SysFileUpload;
import cn.superlu.s3uploadservice.model.vo.BaseFileVo;
import cn.superlu.s3uploadservice.model.vo.UploadUrlsVO;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

public interface SysFileUploadService {

    R<BaseFileVo<FileUploadInfo>> checkFileByMd5(String md5);

    R<BaseFileVo<UploadUrlsVO>> initMultipartUpload(FileUploadInfo fileUploadInfo);

    R<BaseFileVo<String>> mergeMultipartUpload(String md5);

    ResponseEntity<byte[]> downloadMultipartFile(Long id, HttpServletRequest request, HttpServletResponse response) throws IOException;

    R<List<SysFileUpload>> getFileList();
}
