import createChunk from '@/core/createChunk.js'

onmessage = async (e: MessageEvent) => {
  console.log("111")
  const { file, CHUNK_SIZE, startChunkIndex: start, endChunkIndex: end } = e.data

  const promise= []
  for (let i = start; i <= end; i++) {
    promise.push(createChunk(file, i, CHUNK_SIZE))
  }
  const chunks = await Promise.all(promise)
  postMessage(chunks)
}

