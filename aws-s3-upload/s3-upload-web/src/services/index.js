import axios from 'axios';
import {ElMessage} from "element-plus";

/**
 * 创建并配置一个 Axios 实例对象
 */
const service = axios.create({
  baseURL: "/api",
  timeout: 50000, // 全局超时时间
  paramsSerializer: {
    serialize: (params) => {
      return qs.stringify(params, {arrayFormat: 'repeat'});
    }
  }
});

/**
 * Axios请求拦截器，对请求进行处理
 * 1. 序列化get请求参数
 * 2. 统一增加Authorization和TENANT-ID请求头
 * 3. 自动适配单体、微服务架构不同的URL
 * @param config AxiosRequestConfig对象，包含请求配置信息
 */
service.interceptors.request.use(
    (config) => {
      // 处理完毕，返回config对象
      console.log("请求信息", config)
      return config;
    },
    (error) => {
      // 对请求错误进行处理
      return Promise.reject(error);
    }
);

/**
 * 响应拦截器处理函数
 * @param response 响应结果
 * @returns
 */
const handleResponse = (response) => {
  if (response.data.code === 1) {
    throw response.data;
  }
  return response.data;
};

/**
 * 添加 Axios 的响应拦截器，用于全局响应结果处理
 */
service.interceptors.response.use(handleResponse, (error) => {
  const { response } = error
  const status = Number(error.response.status) || 200;
  if (error.message.indexOf('timeout') !== -1) ElMessage.error('请求超时，请稍后再试')
  if (response) ElMessage.error(response.statusText)
  return Promise.reject(error.response.data);
});

// 导出 axios 实例
export default service;
