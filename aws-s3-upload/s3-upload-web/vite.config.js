import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { loadEnv } from 'vite'
export default ({ command, mode }) => {
  // ↓加载环境变量
  const env = loadEnv(mode, process.cwd())
  const __DEV__ = mode === 'development'
  const alias = {
    '@': resolve(__dirname, 'src'),
  }
  return {
    // ↓插件配置
    plugins: [
      vue(),
    ],
    // ↓解析配置
    resolve: {
      // ↓路径别名
      alias
    },
    // ↓服务端配置
    server: {
      //......
      port: 4000,         //vite的默认端口（别和后端冲突了）
      proxy: {
        "/api": {         //代理的请求
          target: "http://localhost:6677",    //后端的地址
          changeOrigin: true,                 //开启跨域访问
          rewrite: (path) => path.replace(/^\/api/,''),    //重写前缀（如果后端本身就有api这个通用前缀，那么就不用重写）
        },
      },
    },
    build:{
      chunkSizeWarningLimit:1000,
      rollupOptions:{
        output:{
          manualChunks(id){ // 分包
            if(id.includes('node_modules')){
              return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
          }
        }
      }
    }
  }
}

