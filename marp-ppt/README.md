# marp-ppt 项目文档
## 项目简介
`marp-ppt` 是一个利用 AI 技术生成 PPT 的服务项目。该项目包含两个主要部分：`marp-ppt-service` 后端服务和 `marp-ppt-web` 前端界面。后端使用 Flask 框架搭建，并集成了 dify 和 ollama AI 接口来实现 PPT 的自动化生成。前端则采用 Vue3 进行开发，提供了一个用户友好的交互界面。

## 项目结构
```
marp-ppt/
├── marp-ppt-service  # 后端服务
└── marp-ppt-web      # 前端项目
└── marp-demo.sql     # mysql脚本
```
## 后端项目（marp-ppt-service）
### 环境要求
- Python 3.12
- Poetry（用于包管理）
### 初始化步骤
1. 克隆项目仓库：
   ```sh
   cd marp-ppt/marp-ppt-service
   ```
2. 使用 Poetry 初始化项目环境：
   ```sh
   poetry install
   ```
### 运行服务
在项目根目录下运行以下命令启动服务：
```sh
poetry run flask run
```
## 前端项目（marp-ppt-web）
### 环境要求
- Node.js 14.0+
- npm 6.0+
### 初始化步骤
1. 进入前端项目目录：
   ```sh
   cd marp-ppt/marp-ppt-web
   ```
2. 使用 npm 初始化项目：
   ```sh
   npm install
   ```
### 开发环境
在项目根目录下运行以下命令启动开发服务器：
```sh
npm run dev
```


### 功能说明
- 提供用户友好的界面，用于输入生成PPT的需求。
- 与后端服务通信，接收生成的PPT文件。

