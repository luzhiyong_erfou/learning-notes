/*
 Navicat Premium Dump SQL

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80300 (8.3.0)
 Source Host           : localhost:3306
 Source Schema         : marp-demo

 Target Server Type    : MySQL
 Target Server Version : 80300 (8.3.0)
 File Encoding         : 65001

 Date: 22/12/2024 13:54:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ppt_template
-- ----------------------------
DROP TABLE IF EXISTS `ppt_template`;
CREATE TABLE `ppt_template` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名字',
  `template_content` text COMMENT '模板内容',
  `template_params` text COMMENT '模板参数',
  `description` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `category` varchar(255) DEFAULT NULL COMMENT '种类',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of ppt_template
-- ----------------------------
BEGIN;
INSERT INTO `ppt_template` (`id`, `name`, `template_content`, `template_params`, `description`, `category`) VALUES ('3231252a265149b299496cab275d6ba7', '科技', '---\nmarp: true\ntheme: gaia\npaginate: true\n_class: lead\n---\n\n<!-- backgroundImage: url(\'https://superlu-demo.oss-cn-beijing.aliyuncs.com/15b5fcebefa240399692b553f1831727.svg\') -->\n\n# {{title}}\n\n---\n\n# 目录\n<!-- backgroundImage: url(\'https://marp.app/assets/hero-background.svg\') -->\n{{menuList}}\n\n---\n{{contents}}\n', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '用户ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` (`id`, `username`, `nick_name`, `password`, `create_time`) VALUES ('7a69c194a99b4df8871aa0d802e1cc94', 'admin', 'Admin', 'e10adc3949ba59abbe56e057f20f883e', '2024-12-20 16:20:21');
INSERT INTO `user` (`id`, `username`, `nick_name`, `password`, `create_time`) VALUES ('ea897b46bdaa48118224c0f5f2cf7c07', 'superlu', 'Cheryl', 'e10adc3949ba59abbe56e057f20f883e', '2024-12-20 16:23:55');
COMMIT;

-- ----------------------------
-- Table structure for user_ppt
-- ----------------------------
DROP TABLE IF EXISTS `user_ppt`;
CREATE TABLE `user_ppt` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `marp` text,
  `ppt_url` varchar(255) DEFAULT NULL COMMENT 'ppt地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user_ppt
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
