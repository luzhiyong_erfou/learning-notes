from flask import Flask
from base import base as base_blueprint
from extensions import db,login_manager # 从 extensions 导入 db

def create_app():
    app = Flask(__name__)
    # mysql数据库库用户名
    username = "root"
    # mysql 数据库密码
    pwd = "Zz961027"
    # mysql 数据库host地址
    ip = "127.0.0.1"
    # mysql 数据库端口
    port = "3306"
    # 代码使用的数据库名
    database = "marp-demo"
    # 设置mysql 链接方法是
    app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{username}:{pwd}@{ip}:{port}/{database}?charset=utf8'
    # 定义应用使用数据库的配置
    # 设置SQLALCHEMY_TRACK_MODIFICATIONS参数 不设置该配置的时候会抛出警告
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    # 将app与flask-SQLAlchemy的db进行绑定
    db.init_app(app)

    # 注册蓝图
    app.register_blueprint(base_blueprint, url_prefix='/api')
    print("初始化")

    # flask-login
    app.config['SECRET_KEY'] = 'ec9439cfc6c796ae2029594d'
    login_manager.init_app(app)

    return app

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)
