from flask import Blueprint

#base = Blueprint('base', __name__, url_prefix='/base')
# 为蓝图设置一个URL前缀（可选），例如 `/base`

base = Blueprint('base', __name__)

from routers import *
