# models/PptTemplate.py
from extensions import db  # 绝对导入 db 实例

# 定义数据模型
class PptTemplate(db.Model):
    __tablename__ = 'ppt_template'
    id = db.Column(db.String(32), primary_key=True)
    name = db.Column(db.String(255))
    template_content = db.Column(db.Text)
    template_params = db.Column(db.Text)
    description = db.Column(db.String(255))
    category = db.Column(db.String(255))

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'templateContent': self.template_content,
            'templateParams': self.template_params,
            'description': self.description,
            'category': self.category
        }