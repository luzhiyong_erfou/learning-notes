# models/User.py
from flask_login import UserMixin
from extensions import (db,login_manager)  # 绝对导入 db 实例


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == user_id).first()

class User(db.Model,UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.String(32), primary_key=True)
    nick_name = db.Column(db.String(255))
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    create_time = db.Column(db.DateTime)

    def to_json(self):
        return {
            'id': self.id,
            'username': self.username,
            'nickName': self.nick_name,
            'createTime': self.create_time
        }