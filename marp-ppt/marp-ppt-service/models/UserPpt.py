# models/UserPpt.py
from extensions import db  # 绝对导入 db 实例

class UserPpt(db.Model):
    __tablename__ = 'user_ppt'
    id = db.Column(db.String(32), primary_key=True)
    user_id = db.Column(db.String(32))
    marp = db.Column(db.Text)
    ppt_url = db.Column(db.String(255))
    create_time = db.Column(db.DateTime)

    def to_json(self):
        return {
            'id': self.id,
            'userId': self.user_id,
            'marp': self.marp,
            'pptUrl': self.ppt_url,
            'createTime': self.create_time
        }