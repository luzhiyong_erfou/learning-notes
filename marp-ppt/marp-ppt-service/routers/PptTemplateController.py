from flask import request
from base import base # 绝对导入 base 蓝图和模型
from models import PptTemplate
from flask import jsonify
from extensions import db
import uuid



@base.route("/ppt/template/list", methods=["GET"])
def get_template_list():
    data_list = PptTemplate().query.all()
    return jsonify({'msg': '操作成功', 'code': 200, 'data': [data.to_json() for data in data_list]})


@base.route("/ppt/template/save",methods=["post"])
def save_template():
    request_json=request.get_json()
    ppt_template=PptTemplate()
    ppt_template.id=str(uuid.uuid4()).replace("-", "")
    if "name" in request_json:ppt_template.name=request_json.get("name")
    if "templateContent" in request_json:ppt_template.template_content=request_json.get("templateContent")
    if "templateParams" in request_json:ppt_template.template_params=request_json.get("templateParams")
    if "description" in request_json:ppt_template.description=request_json.get("description")
    if "category" in request_json:ppt_template.category=request_json.get("category")

    db.session.add(ppt_template)
    db.session.commit()
    return jsonify({'msg': '操作成功', 'code': 200, 'data':ppt_template.to_json()})




