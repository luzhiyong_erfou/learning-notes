from ollama import Client 

class OllamaClient(Client):
     def __init__(self, host: str = "http://127.0.0.1:11434"):
        super().__init__(host=host)  # 调用父类的初始化方法


     def create_chat_message(self, model='glm4:9b',system_prompt='', user_input=''):
        # 创建聊天消息
        response = self.chat(
            model=model,
            messages=[
                {"role": "system", "content": system_prompt},
                {"role": "user", "content": user_input}
            ]
        )
        return response
       
