import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import App from './App.vue'
import router from './router'; // 导入路由配置
// css
import './style.css'
import 'element-plus/dist/index.css'
import 'quasar/dist/quasar.css';
import { Quasar } from 'quasar';
const app = createApp(App)
app.use(router) // 使用路由配置
app.use(ElementPlus)
app.use(Quasar)
app.mount('#app')
