// src/router/index.js
import axios from "axios";
import component from "element-plus/es/components/tree-select/src/tree-select-option.mjs";
import { createRouter, createWebHistory } from "vue-router";

// 定义路由
const routes = [
    { 
        path: "/",
        component: () => import("../views/Home.vue"),
    },
    { 
        path: "/login",
        component: () => import("../views/user/login.vue"),
    },
    { 
        path: "/register",
        component: () => import("../views/user/register.vue"),
    },
    {
        path:"/template/add",
        component:()=>import("../views/template/add.vue")
    }
];

// 创建路由器实例
const router = createRouter({
    history: createWebHistory(),
    routes,
});
router.beforeEach((to, from, next) => {
    if (to.path === "/login" || to.path === "/register") {
        next();
    } else {
        axios.get("/api/api/user/check_login").then((res) => {
            res.data.data.logged_in ? next() : next("/login");
        }).catch((err) => {
            next("/login")
        });
    }
   
});
// 导出路由器
export default router;
