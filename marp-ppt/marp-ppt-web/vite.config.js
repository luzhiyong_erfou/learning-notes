import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vite.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    host: '0.0.0.0', // 允许外部访问
    port: 3000, // 设置开发服务器端口
    open: true, // 自动打开浏览器
    proxy: {
      '/api': {
        target: 'http://localhost:5000', // 代理到后端服务
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})